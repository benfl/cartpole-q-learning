import gym
import logging
import numpy as np


class QLearner:
    """
    Run classical Q-learning to try to solve the "cartpole" problem.
    See e.g. https://en.wikipedia.org/wiki/Q-learning
    """

    def __init__(self, start_learning: float, end_learning: float,
                 learning_decay_steps: int, start_exploration: float,
                 end_exploration: float, exploration_decay_steps: int,
                 gamma: float, init_value: float) -> None:
        """
        start_learning: initial learning rate
        end_learning: final learning rate
        learning_decay_steps: how many steps to decay learning rate
        start_exploration: initial exploration factor
        end_exploration: final exploration factor
        exploration_decay_steps: how many steps to decay exploration
        gamma: discount factor
        init_value: initial guess for the value of each state
        """
        self.count = 0  # number of iterations
        self.start_learning = start_learning
        self.end_learning = end_learning
        self.learning_decay_steps = learning_decay_steps
        self.start_exploration = start_exploration
        self.end_exploration = end_exploration
        self.exploration_decay_steps = exploration_decay_steps
        self.gamma = gamma

        self.env = gym.make('CartPole-v1')
        # Value approximations. There are sixteen states, corresponding to the
        # signs of the four observed values.
        self.Q = init_value * np.ones([2 ** self.env.observation_space.shape[0],
                                       self.env.action_space.n])

    @property
    def alpha(self) -> float:
        """
        Learning rate; decay linearly.
        """
        if self.count < self.learning_decay_steps:
            return self.start_learning - ((self.start_learning - self.end_learning)
                                          * self.count / self.learning_decay_steps)
        else:
            return self.end_learning

    @property
    def epsilon(self) -> float:
        """
        Exploration factor; decay linearly.
        """
        if self.count < self.exploration_decay_steps:
            return self.start_exploration - ((self.start_exploration - self.end_exploration)
                                             * self.count / self.exploration_decay_steps)
        else:
            return self.end_exploration

    @staticmethod
    def discretize(obs: np.ndarray) -> int:
        """
        Convert a state observation into a number between 0 and 15.
        The binary representation is 0 in the bit corresponding to 2^i
        if obs[i] <= 0; otherwise it is 1.

        obs: length 4 array of observation
        """
        return sum((obs[i] > 0) * 2 ** i for i in range(len(obs)))

    def get_action(self, disc_obs: int) -> int:
        """
        Return the action chosen by Q-learning.

        disc_obs: discretized observation, as returned from discretize method
        """
        if np.random.rand() < self.epsilon:
            # Exploring
            return self.env.action_space.sample()
        # Pick a random action out of the ones with maximum estimated value.
        return np.random.choice(np.flatnonzero(self.Q[disc_obs] == self.Q[disc_obs].max()))

    def simulate(self, visualize: bool = False) -> int:
        """
        Simulate one round of the cartpole and return the number of time
        steps the pole managed to stay upright.

        visualize: include a visualization of the cart
        """
        obs = self.env.reset()
        disc_obs = self.discretize(obs)
        action = self.get_action(disc_obs)
        for t in range(500):
            # Simulation ends after 500 time steps.
            old_disc = disc_obs
            obs, reward, done, _ = self.env.step(action)
            if visualize:
                self.env.render()

            disc_obs = self.discretize(obs)
            next_action = self.get_action(disc_obs)

            # Estimate value and update Q-array.
            est = reward + self.gamma * np.max(self.Q[disc_obs])
            self.Q[old_disc, action] += self.alpha * (est - self.Q[old_disc, action])
            action = next_action
            if done:
                self.env.close()
                self.count += 1
                if t < 499:
                    # If not on last time step, update Q-array to reflect failure (zero reward).
                    self.Q[disc_obs, next_action] -= self.alpha * self.Q[disc_obs, next_action]
                return t + 1
                break
        logging.warning('Something went wrong---simulation went for too many timesteps!')
        self.env.close()
        return 500


if __name__ == '__main__':
    learner = QLearner(0.3, 0.01, 5000, 1, 0.01, 2000, 1, 100)
    total_time = 0
    best_time = 0
    for i in range(5000):
        if i % 1000 == 999:
            time = learner.simulate(visualize=True)
        else:
            time = learner.simulate()
        total_time += time
        best_time = max(best_time, time)
        if i % 100 == 99:
            print(f'Average time for last 100 runs: {total_time / 100}. Best time: {best_time}.')
            total_time = 0
            best_time = 0
