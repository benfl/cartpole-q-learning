# Cartpole Q-learning

A simple implementation of classical [Q-learning](https://en.wikipedia.org/wiki/Q-learning) for the [cartpole](https://gym.openai.com/envs/CartPole-v1/) problem.  Discretize the state space into sixteen states in order to be able to apply the basic algorithm.

Output from a sample run:
```
Average time for last 100 runs: 22.62. Best time: 68.
Average time for last 100 runs: 22.59. Best time: 58.
Average time for last 100 runs: 24.47. Best time: 85.
Average time for last 100 runs: 22.84. Best time: 72.
Average time for last 100 runs: 25.85. Best time: 69.
Average time for last 100 runs: 27.28. Best time: 131.
Average time for last 100 runs: 26.99. Best time: 91.
Average time for last 100 runs: 27.7. Best time: 132.
Average time for last 100 runs: 25.7. Best time: 109.
Average time for last 100 runs: 28.12. Best time: 134.
Average time for last 100 runs: 32.67. Best time: 165.
Average time for last 100 runs: 29.97. Best time: 140.
Average time for last 100 runs: 38.61. Best time: 148.
Average time for last 100 runs: 38.7. Best time: 192.
Average time for last 100 runs: 41.3. Best time: 190.
Average time for last 100 runs: 49.1. Best time: 444.
Average time for last 100 runs: 51.44. Best time: 227.
Average time for last 100 runs: 51.51. Best time: 301.
Average time for last 100 runs: 54.18. Best time: 217.
Average time for last 100 runs: 74.05. Best time: 278.
Average time for last 100 runs: 130.55. Best time: 263.
Average time for last 100 runs: 107.5. Best time: 265.
Average time for last 100 runs: 117.56. Best time: 250.
Average time for last 100 runs: 163.61. Best time: 500.
Average time for last 100 runs: 201.89. Best time: 500.
Average time for last 100 runs: 76.6. Best time: 276.
Average time for last 100 runs: 77.71. Best time: 443.
Average time for last 100 runs: 122.03. Best time: 500.
Average time for last 100 runs: 127.24. Best time: 409.
Average time for last 100 runs: 82.38. Best time: 240.
Average time for last 100 runs: 314.99. Best time: 500.
```

It certainly improves from the start (staying alive for over 300 timesteps compared to about 20). Further improvements could surely be made by varying the parameters.
